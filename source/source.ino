#if defined(ESP32)
#include <WiFiMulti.h>
WiFiMulti wifiMulti;
#define DEVICE "ESP32"
#elif defined(ESP8266)
#include <ESP8266WiFiMulti.h>
ESP8266WiFiMulti wifiMulti;
#define DEVICE "ESP8266"
#endif

//files from the v3 factory display test project
#include "Arduino.h"
#include "WiFi.h"
#include <Wire.h>
#include "HT_SSD1306Wire.h"

SSD1306Wire  factory_display(0x3c, 500000, SDA_OLED, SCL_OLED, GEOMETRY_128_64, RST_OLED); // addr , freq , i2c group , resolution , rst


#include <InfluxDbClient.h>
#include <InfluxDbCloud.h>
#include "time.h"

//#define BMEONLY  //uncomment if not using the rangefinder/analog input
#ifdef BMEONLY
  #define updateInterval 120000
#else
  #define updateInterval 600000
#endif

unsigned long lastMillisTime=0;  //controls how frequently the time is checked for the automatic count reset
int currentHour;  //stores the current hour. will be used to reset the counter at the end of each day.
int currentMin;   //stores current minute - used for discarding delays during scheduled breaks
int previousHour;
unsigned long msPerMirror=0;   //milliseconds each mirror took to pass sensor. used to estimate mirror size and correlate with production rate.
unsigned long timeBetweenMirrors=0;  //milliseconds between each mirror
float timePerMirror=0.0;  //variable to store millis()-msPerMirror
int runonce=1;   //used to prevent false triggers by making sure mirror is still there after a time check
float feedrate = 5.3;  //inches per second for rollers
int mirrorSize=0;  //calculated mirror size based on timePerMirror and feedrate
bool failedWrite=false;  //if midnight reset fails to write, re-send data
int sendDataLoopCount=0;    //used to sequentially go through items in the 10-minute delay to minimize downtime

//Libraries for OLED Display
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <Adafruit_Sensor.h>
#include "Adafruit_BME680.h"

#define SEALEVELPRESSURE_HPA (1013.25)

//Machine Variables
#define Version "V1.0.4"
String machine_name = "Blast_Booth_1";  //update #define on next line to match schedules below when changing this
#define laserOnly            //used to limit the number of fields sent to influx

const char* ntpServer = "pool.ntp.org";
const long gmtOffset_sec = -28800;
const int daylightOffset_sec = 3600;
unsigned long counter = 0;  //displays current count since last reset
unsigned int dailyCount = 0;  //stores total count at end of day
bool breaktimeReset = false;  //used to restrict sending the time between mirrors if coming back from a break

//Break Schedules for different departments/machines defined here {start hour, start min, end hour, end min}
#define laserSchedule
#ifdef laserSchedule
  int breakSchedule[3][4]={{8,0,8,15}, {10,30,11,00}, {13,30,13,45}};         //{{8,0,8,15}, {10,30,11,0}, {14,55,16,25}};
#endif
#ifdef doubleEdger
  int breakSchedule[3][4]={{8,0,8,15}, {10,30,11,00}, {13,30,13,45}};  //specify a break schedule for a specific machine to ignore downtime during this period
#endif

//Setup IR Sensor
#define inPin0 2 // Sensor connected to analog pin
int input0 = 0; // variable to store the read value
//#define resetPin GPIO_NUM_13 // pin used to reset board at end of day (Doesn't work; GPIO can't sink enough current to reset board. Need to use relay for this feature)

//state change variables
int input0voltage = 0; // analog voltage
int voltageThreshold = 2000;  //voltage threshold for use in object detection-2000 is about 8-10" with the test sensor
int input0State = 0; // current state of the input
int lastinput0State = 0; // previous state of the input

//local time information
struct tm timeinfo;

//display on microcontroller
//Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RST);

// WiFi AP SSID
#define WIFI_SSID "EM_GUEST"
// WiFi password
#define WIFI_PASSWORD "ElectricMirror.com"
// InfluxDB v2 server url, e.g. https://eu-central-1-1.aws.cloud2.influxdata.com (Use: InfluxDB UI -> Load Data -> Client Libraries)
#define INFLUXDB_URL "https://us-west-2-1.aws.cloud2.influxdata.com"
// InfluxDB v2 server or cloud API authentication token (Use: InfluxDB UI -> Data -> Tokens -> <select token>)
#define INFLUXDB_TOKEN "AgeMpTnLeuT2nZYxmyjLQRJqFvfuM50yoMgLSyOOPKeksALOjJ3mJVQePPv-MJWRuW2WWyMSz8q2m7gu8NC6Mg=="
// InfluxDB v2 organization id (Use: InfluxDB UI -> User -> About -> Common Ids )
#define INFLUXDB_ORG "larry.zerr@gmail.com"
// InfluxDB v2 bucket name (Use: InfluxDB UI -> Data -> Buckets)
#define INFLUXDB_BUCKET "testing"

// Set timezone string according to https://www.gnu.org/software/libc/manual/html_node/TZ-Variable.html
// Examples:
// Pacific Time: "PST8PDT"
// Eastern: "EST5EDT"
// Japanesse: "JST-9"
// Central Europe: "CET-1CEST,M3.5.0,M10.5.0/3"
#define TZ_INFO "CET-1CEST,M3.5.0,M10.5.0/3"

// InfluxDB client instance with preconfigured InfluxCloud certificate
InfluxDBClient client(INFLUXDB_URL, INFLUXDB_ORG, INFLUXDB_BUCKET, INFLUXDB_TOKEN, InfluxDbCloud2CACert);

// Data point
Point sensor(machine_name);

// bme680 stuff
Adafruit_BME680 bme; // I2C

//these two functions may be what turns the display on or off
void VextON(void)
{
  pinMode(Vext,OUTPUT);
  digitalWrite(Vext, LOW);
}
void VextOFF(void) //Vext default OFF
{
  pinMode(Vext,OUTPUT);
  digitalWrite(Vext, HIGH);
}

void updateDisplay(void)
{
  factory_display.clear();
  
  factory_display.drawString(0,0,"EM Logger "+(String)Version);
  factory_display.drawString(0,9,machine_name);
  factory_display.drawString(0,18,"WIFI SSID "+(String)WiFi.SSID());
  factory_display.drawString(0,30,"WIFI SIGNAL "+(String)WiFi.RSSI());
  if (input0State == 0){
    factory_display.drawString(40,40,"SANDBLAST OFF");  //not detected
  }
  else {
    factory_display.drawString(0,40,"SANDBLAST ON"); //detected
  }
  factory_display.drawString(0,50,"Count: "+(String)counter);
  
  factory_display.display();
}

void setup() {
  Serial.begin(115200);

  //Initialize Sensor Input
  pinMode(inPin0, INPUT); // sets the pin as input
  
  //turn on display and initialize
  VextON();
  delay(100);
  factory_display.init();
  factory_display.clear();
  factory_display.display();

  updateDisplay();

  /*
  if (!bme.begin()) {
    Serial.println(F("Could not find a valid BME680 sensor, check wiring!"));
  //while (1);
  }
  */

  // Set up oversampling and filter initialization
  bme.setTemperatureOversampling(BME680_OS_8X);
  bme.setHumidityOversampling(BME680_OS_2X);
  bme.setPressureOversampling(BME680_OS_4X);
  bme.setIIRFilterSize(BME680_FILTER_SIZE_3);
  bme.setGasHeater(320, 150); // 320*C for 150 ms

  // Setup wifi
  WiFi.mode(WIFI_STA);
  wifiMulti.addAP(WIFI_SSID, WIFI_PASSWORD);

  Serial.println("Connecting to wifi:");
  while (wifiMulti.run() != WL_CONNECTED) {
      Serial.println(".");
      delay(7000);
  }

  Serial.print("Connected to ");
  Serial.println(WIFI_SSID);

  printAPlist(1);  //print out list of all APs and their signal strength

  // Add tags
  sensor.addTag("D", DEVICE);
  sensor.addTag("V", Version);
  //sensor.addTag("SSID", WiFi.SSID());

  // Accurate time is necessary for certificate validation and writing in batches
  // For the fastest time sync find NTP servers in your area: https://www.pool.ntp.org/zone/
  // Syncing progress and the time will be printed to Serial.
  timeSync(TZ_INFO, ntpServer, "time.nis.gov");

  // Check server connection
  if (client.validateConnection()) {
      Serial.print("Connected to InfluxDB: ");
      Serial.println(client.getServerUrl());
  } else {
      Serial.print("InfluxDB connection failed: ");
      Serial.println(client.getLastErrorMessage());
  }

  sensor.clearFields();
  //write initial count of 0
  #ifndef BMEONLY
  sensor.addField("count", counter);
  #endif
  sensor.addField("rssi", WiFi.RSSI());

  // Write point
  Serial.print("Writing: ");  //print what is being sent to influx
  Serial.println(sensor.toLineProtocol());

  if (!client.writePoint(sensor)) {
    Serial.print("InfluxDB write failed.: ");
    Serial.println(client.getLastErrorMessage());
  }
  sensor.clearFields();  //clear fields after writing

  configTime(gmtOffset_sec, daylightOffset_sec, ntpServer);
}


void loop() {
  //Grab input data from sensors
  input0voltage = analogRead(inPin0); // read the input pin

  //for tuning voltage threshold for detection-can be dialed in so it won't detect objects much farther away than the desired distance
  // Serial.print("voltage: ");
  // Serial.println(input0voltage);

  //read voltage to determine if there is an object near the sensor
  if(input0voltage > voltageThreshold){
    if(runonce){
      runonce=0;
      msPerMirror=millis();  //get time sensor was first broken
      Serial.print("Sensor: ");
      Serial.println(1);
    }
    if(millis()-msPerMirror > 750){  //if sensor is reading high for 0.75 second, advance to test
      input0State=1;
    }
  }
  else{   //voltage below threshold
    if(runonce==0){
      Serial.print("Sensor: ");
      Serial.println(0);
    }
    runonce=1;
    input0State=0;
  }

  //detect distance sensor state change and log to influx
  if (input0State != lastinput0State) {

    if(input0State == 1){

      if (++counter > 1  && breaktimeReset == false){     //only add time between mirrors if not first mirror of day or first mirror back from a break
        sensor.addField("secondsBetween", (millis()-timeBetweenMirrors)/1000);
      }
      else{
        breaktimeReset=false;  //clear the break time flag so it will report time between mirrors on the next mirror
      }
      sensor.addField("count", counter);
      sensor.addField("sensor", 1);  //state of distance sensor 1-object detected 0-nothing detected
      
      updateDisplay();
    
    }else{  //no mirror present

      timeBetweenMirrors=millis();  //record time when mirror passed
      timePerMirror=(millis()-msPerMirror)/1000.0;  //stores how long mirror took to pass sensor in ms
      sensor.addField("timePerMirror", timePerMirror);  //record time each mirror took to pass sensor
      #ifndef laserOnly
      mirrorSize=timePerMirror*feedrate;  //converts time to mirror size in inches

      sensor.addField("mirrorSize", mirrorSize);  //record mirror size in inches
      #endif
      sensor.addField("count", counter);
      sensor.addField("sensor", 0);
      
      updateDisplay();
    }

    // Write point
    Serial.print("Writing: ");   //print what is being sent to influx
    Serial.println(sensor.toLineProtocol());

    if (!client.writePoint(sensor)) {
      Serial.print("InfluxDB write failed,: ");
      Serial.println(client.getLastErrorMessage());
    }
    sensor.clearFields();  //clear fields after writing
  }

  lastinput0State = input0State;
  


  //10 second delay
  if (((millis()-lastMillisTime) % 10000) < 10){  //update wifi signal strength every 10 seconds

    updateDisplay();

    Serial.print("WiFi.RSSI(): ");
    Serial.println(WiFi.RSSI());

    
    if (WiFi.RSSI() == 0){  //if wifi signal has been lost, attempt to reconnect
      Serial.println("Wifi signal lost - disconnecting");
      WiFi.disconnect();
      
      if(WiFi.begin(WIFI_SSID, WIFI_PASSWORD) == WL_CONNECTED){
        Serial.println("Reconnected");
      }
      else{
        Serial.println("Attempting to reconnect");
      }
    }
  }
 
  //10 minute delay
  if(millis()-lastMillisTime > updateInterval)  //check time every 10 minutes-used for counter reset and wifi signal checking 
  {
    unsigned long downtime=millis();

    if (input0voltage < voltageThreshold) {  //only do these time-expensive things if the sensor isn't reading anything

      if (WiFi.RSSI() < -80){  //if signal is pretty bad
        printAPlist(2);      //check if there is a better signal (takes several seconds to complete)
      }

      if(sendDataLoopCount==0) {   //first take bme680 readings and record wifi signal strength

      #ifdef BMEONLY
        int bmeFault=0;
        // Tell BME680 to begin measurement.
        unsigned long endTime = bme.beginReading();
        if (endTime == 0) {
          Serial.println(F("BME didn't begin"));
          bmeFault=1;
          //return;
        }
        else {
          bmeFault=0;
        }

        // Obtain measurement results from BME680. Note that this operation isn't
        // instantaneous even if milli() >= endTime due to I2C/SPI latency.
        if (!bme.endReading()) {
          Serial.println(F("BME didn't finish"));
          bmeFault=1;
          //return;
        }
        else {
          bmeFault=0;
        }


        if (bmeFault==0) {  //print the BME data over serial
          float tempC = bme.temperature;
          Serial.print(F("*C = "));
          Serial.println(tempC);
          sensor.addField("tempC", tempC);

          float hPa = bme.pressure / 100.0;
          Serial.print(F("hPa = "));
          Serial.println(hPa);
          sensor.addField("hPa", hPa);

          float humid = bme.humidity;
          Serial.print(F("Hum(%) = "));
          Serial.println(humid);
          sensor.addField("hum", humid);

          float gas = bme.gas_resistance / 1000.0;
          Serial.print(F("Gas = "));  //Kohms
          Serial.println(gas);
          sensor.addField("gas", gas);

          // Serial.print(F("Approx. Altitude = "));
          // Serial.print(bme.readAltitude(SEALEVELPRESSURE_HPA));
          // Serial.println(F(" m"));

          //if not using sensor as a counter, write bme680 information on LED display
          
          /*  this section needs to be revised if the BME680 is to be used again 
          display.setCursor(1,40);
          display.setTextColor(WHITE,BLACK);
          display.print("T:");
          display.print(tempC);
          display.print("C");
          display.print(" H:");
          display.print(humid);
          display.print("%");
          display.setCursor(1,50);
          display.print("P:");
          display.print(hPa);
          display.print("hPa");          
          display.print(" G:");
          display.print(gas);
          display.print("k");
          display.display();
          */
          
        }  //takes an average of 369ms up to this part if using BME680
        #endif
        sensor.addField("rssi", WiFi.RSSI());  //write wifi signal strength
      }

      else if(sendDataLoopCount==1) {  //on next loop cycle, write data to influx
        Serial.print("Writing: ");   //print what is being sent to influx over serial
        Serial.println(sensor.toLineProtocol());
        
        if (!client.writePoint(sensor)) {
          Serial.print("InfluxDB write failed!: ");
          Serial.println(client.getLastErrorMessage());
        }
        sensor.clearFields();  //clear fields after writing
      }

      else if(sendDataLoopCount==2) {  //then check time on this loop cycle
        printLocalTime();   //print out the local time, check for scheduled breaks, and reset counter at end of day
        lastMillisTime=millis();  //reset 10 min timer
      }

      Serial.print(sendDataLoopCount);
      Serial.print(" downtime (ms): ");   //report how much time was lost in each phase of this section
      Serial.println(millis()-downtime);

      sendDataLoopCount++;  //advance to next phase for next loop cycle (update input0voltage each time)
      if(sendDataLoopCount>2) {
        sendDataLoopCount=0;
      }
    }
  }
  
  delay(10);  //10ms second delay between sensor checks
  //Serial.print(".");  //for seeing how long delays between scans are
}


void printLocalTime(){   //gets local time and writes to currentHour/previousHour variables
  
  if(!getLocalTime(&timeinfo)){  //update time
    Serial.println("Failed to obtain time");
    return;
  }
  else{
    // debug Serial.println(&timeinfo, "%A, %B %d %Y %H:%M:%S");
    currentHour=timeinfo.tm_hour;
  }
  
  //Serial.println(currentHour);  //for debugging
  //Serial.println(previousHour);

  //check for break time, if so do not send seconds between mirrors
  for (int i=0; i<3; i++){
    //if current hour is the same as break time start and end hour
    if(currentHour == breakSchedule[i][0]  &&  currentHour == breakSchedule[i][2]){
      if(timeinfo.tm_min > breakSchedule[i][1]  && timeinfo.tm_min < breakSchedule [i][3]){  //if minutes are between schedule range
        Serial.println("Scheduled break time - discarding time between mirrors");
        breaktimeReset=true;
        printAPlist(2);  //force connection to strongest wifi signal
      }
    }
    //if current hour is within the bounds of break time start and end hours
    else if (currentHour == breakSchedule[i][0]  && currentHour < breakSchedule[i][2]){
      if (timeinfo.tm_min > breakSchedule[i][1]) {
        Serial.println("Scheduled break time - discarding time between mirrors");
        breaktimeReset=true;
        printAPlist(2);  //force connection to strongest wifi signal
      }
    }
    else if (currentHour > breakSchedule[i][0]  &&  currentHour == breakSchedule[i][2]){
      if (timeinfo.tm_min < breakSchedule[i][3]){
        Serial.println("Scheduled break time - discarding time between mirrors");
        breaktimeReset=true;        
        printAPlist(2);  //force connection to strongest wifi signal
      }
    }
  }


  //at end of day, reset counter to zero and check for strongest wifi connection
  if((currentHour != previousHour) || failedWrite){
    if((currentHour==0 && previousHour==23) || failedWrite){  //this should be local time, hour 0 is midnight
      if (counter == 0  && failedWrite == false){  //weekend or day off, clear dailyCount
        dailyCount = 0;
      }
      else if (counter > 0){
        dailyCount=counter;  //store daily count value if count is greater than 0
      }
      counter=0;  //reset counter
      Serial.println("New day: resetting count and connecting to strongest wifi");
      
      printAPlist(2);  //force connection to strongest wifi signal

      // Store measured value into point
      // Report RSSI of currently connected network
      sensor.addField("rssi", WiFi.RSSI());

      #ifndef BMEONLY  //don't add these fields if only using for BME data
      sensor.addField("dailyCount", dailyCount);
      sensor.addField("count", counter);
      #endif

      Serial.print("Writing: ");  //print what is being sent to influx
      Serial.println(sensor.toLineProtocol());

      if (!client.writePoint(sensor)) {  //write data to influx
        Serial.print("InfluxDB write failed;: ");
        Serial.println(client.getLastErrorMessage());
        failedWrite=true;  //resend data on next cycle if it failed to send
      }
      else {   //write succeeded
        failedWrite=false;
        sensor.clearFields();  //clear fields after writing
      }
    }
    previousHour=timeinfo.tm_hour;
  }
}


void printAPlist(int option){  //0 to print only networks for provided SSID (if network is 6db better it will connect to that one), 1 to print all visible access points, 2 to force connection to strongest
  int loopRssi=0;
  unsigned long aptime = millis();
  int numSsid = WiFi.scanNetworks();  //get numbered list of available networks (takes about 4 to 5 seconds on the local network)
  //int channel=0;

  int currentSignal = 0;  //current wifi signal strength
  
  Serial.print("Connected to: ");
  Serial.println(WiFi.BSSIDstr());
  
  Serial.println();
  Serial.print("Available APs for ");
  Serial.print(WIFI_SSID);
  Serial.println(":");

  for (int loopNet = 0; loopNet<numSsid; loopNet++){
    String loopSSID = String(WiFi.SSID(loopNet));
    //channel = WiFi.channel(loopNet);
    loopRssi = WiFi.RSSI(loopNet);  //WiFi.RSSI automatically sorts from best signal to worst
    currentSignal = WiFi.RSSI();    //store current wifi signal strength for this loop iteration (prevents switching to second or third best AP)
    
    //Serial.println(WiFi.BSSIDstr() != WiFi.BSSIDstr(loopNet));  //for debugging

    if(loopSSID == WIFI_SSID){   //if ssid is the one defined at beginning of program
      if((loopRssi > currentSignal+5) || (option == 2)){    //if signal is 6db better or if function input is 2, connect to new AP
        if((WiFi.BSSIDstr() != WiFi.BSSIDstr(loopNet))){  //if mac address is not the same as one with best signal, connect to this AP
          WiFi.disconnect();
          delay(10);
          WiFi.begin(WIFI_SSID, WIFI_PASSWORD, loopNet);
          Serial.print("Switching to AP ");
          Serial.println(loopNet);
          break;
        }
        else if (option == 2){   //different mac address and signal is less than 6db better
          Serial.println("Already connected to best AP");
          break;  //don't waste time looking for a better signal
        }
      }  //if none of these cases, print full list

      if(loopNet<10){
        Serial.print("0");
      }
      Serial.print(loopNet);
      //Serial.print("  Ch: ");
      //Serial.print(channel);
      Serial.print("  RSSI: ");
      Serial.print(loopRssi);
      Serial.print("  AP: ");
      Serial.println(WiFi.BSSIDstr(loopNet));
    }
  }

  if(option == 1){    //prints all available access points
    Serial.println();
    Serial.println("All APs:");

    for (int loopNet = 0; loopNet<numSsid; loopNet++){
      String loopSSID = String(WiFi.SSID(loopNet));
      //channel = WiFi.channel(loopNet);
      loopRssi = WiFi.RSSI(loopNet);

      if(loopNet<10){
        Serial.print("0");
      }
      Serial.print(loopNet);
      //Serial.print("  Channel: ");
      //Serial.print(channel);
      Serial.print("  RSSI: ");
      Serial.print(loopRssi);
      Serial.print("  SSID: ");
      Serial.println(loopSSID);
    }
    //Serial.println();
  }
  Serial.print("AP list runtime (ms): ");
  Serial.println(millis()-aptime);
  Serial.println();
}
